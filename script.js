var botao=document.querySelector(".botao")
var botaoCalc=document.querySelector(".calcButton")
var qntNotas=0
var somaNotas=0


// função para pegar a nota que ta na caixa e verificar se é válida
function pegaNota(){
    let nota=document.querySelector(".nota").value
    nota=parseFloat(nota)
    var notaValida = !((!(nota>=0)&&!(nota<=0))||(nota>10)||nota<0)
    if(!notaValida){
        alert('Insira um número de 1 a 10!!')
    } 
    if(notaValida){
        return nota
    }
}

// função para printar a linha, ela também soma todas as notas e mantém a quantidade de notas registradas
function printLinha(){
    var notas=document.querySelector(".notas")
    let nota=pegaNota()
    if(nota!=undefined){
        qntNotas+=1
        somaNotas+=nota
        notas.innerHTML=notas.innerHTML+'A nota '+qntNotas+' é '+nota+'\n'
    }
}

// função para printar a média embaixo da calculadora
function printMedia(){
    let mediaTexto = document.querySelector('h2')
    mediaTexto.innerHTML='A média é: '+somaNotas/qntNotas
}

// Event listeners que realizam as suas respectivas funções quando clicados
botaoCalc.addEventListener("click",printMedia)
botao.addEventListener("click",printLinha)